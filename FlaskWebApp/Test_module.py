import ssl, os, urllib.request, re

url = "https://lenta.ru/"
context = ssl._create_unverified_context()
content = urllib.request.urlopen(url, context=context).read().decode('utf-8')
imgUrls = re.findall('img .*?src="(.*?)"', content)
count = 1
try:
    for img in imgUrls:
        url = img
        img = urllib.request.urlopen(url, context=context).read()
        out = open("./files/{0}.jpg".format(count), "wb")
        out.write(img)
        out.close
        count += 1
except ValueError:
    pass
total = count