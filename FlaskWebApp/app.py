from flask import Flask, render_template, request
import ssl, os, urllib.request, re, datetime, urllib.parse
from Crawler import Crawler
from DirectoryGenerating import DirGen
from Poster import Poster
from wtforms import TextAreaField, validators, Form, StringField

app = Flask(__name__)
app.config.from_object('config')

dirgen = DirGen()
crawler = Crawler()
poster = Poster()

datedirname = datetime.datetime.now().strftime("%Y-%m-%d")

class UrlForm(Form):
    urlInput = StringField('Url', [validators.URL(require_tld=True, message=None)])

@app.route('/', methods=['GET','POST'])
def main():
    form = UrlForm(request.form)
    if request.method == 'POST' and form.validate():
        #return (form.urlInput.data)
        surl = urllib.parse.urlsplit(form.urlInput.data)[1]
        # Check date dir
        dirgen.create(datedirname)
        # Check url dir
        urldir = dirgen.create(datedirname + '/' + surl)
        total = crawler.crawl(urldir, form.urlInput.data)
        return render_template('index.html', form=form)
    else:
        return render_template('index.html', form=form)

if __name__ == '__main__':
    app.run()
