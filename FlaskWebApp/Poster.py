import os, requests, datetime

class Poster:
    def post(self):
        dirname = datetime.datetime.now().strftime("%Y-%m-%d")
        url = "http://172.19.12.51:9080/powerai-vision/api/dlapis/"+"774ffdce-843c-4133-8e15-5f78922519d0"
        path = './files/'+dirname+'/'

        payload = {
            'Content-Disposition': 'form-data',
            'name': 'files',
        }
        headers = {
            'content-type': "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
            'Cache-Control': "no-cache",
            'Postman-Token': "79772d47-f3fc-4077-9dba-17fc9d53f031"
            }

        for filename in os.listdir(path):
            files = {
                'files': open(path+filename, 'rb')
            }
            response = requests.post(url, files=files)
            data = response.json()
            print (data['classified'])