import re, urllib.request, ssl

class Crawler:
    def crawl(self, dir, url):
        try:
            context = ssl._create_unverified_context()
            resource = urllib.request.urlopen(url, context=context)
            content = resource.read().decode(resource.headers.get_content_charset())
            # content = urllib.request.urlopen(url, context=context).read().decode('utf-8')
            imgUrls = re.findall('img .*?src="(.*?)"', content)
            count = 1
            try:
                for img in imgUrls:
                    url = img
                    img = urllib.request.urlopen(url, context=context).read()
                    out = open("./files/"+dir+"/{0}.jpg".format(count), "wb")
                    out.write(img)
                    out.close
                    count += 1
                total = count
                return total
            except ValueError:
                pass
        except UnicodeDecodeError as error:
            pass
